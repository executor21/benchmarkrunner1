//
//  Constants.h
//  BenchmarkRunner1
//
//  Created by Lev Trubov on 4/10/16.
//  Copyright © 2016 Lev Trubov. All rights reserved.
//

#ifndef Constants_h
#define Constants_h

#define BENCHMARK_TEST_RUNTIME 10.0
#define BENCHMARK_INTEGER_SORT_ARRAY_SIZE 100000
#define BENCHMARK_INTEGER_SORT_LOWER_LIMIT -100
#define BENCHMARK_INTEGER_SORT_UPPER_LIMIT 100
#define BENCHMARK_FIBONACCI_EXTENT 35
#define BENCHMARK_MATRIX_SIZE 200
#define BENCHMARK_MATRIX_DIAG_VALUE 2.001
#define BENCHMARK_MATRIX_NON_DIAG_VALUE 1.001
#define BENCHMARK_MATRIX_EPS 0.0000001

#define BENCHMARK_MM_SIZE 200
#define BENCHMARK_MM_LL -100
#define BENCHMARK_MM_UL 100

#endif /* Constants_h */
