//
//  BenchmarkTest.m
//  BenchmarkRunner1
//
//  Created by Lev Trubov on 4/10/16.
//  Copyright © 2016 Lev Trubov. All rights reserved.
//

#import "BenchmarkTest.h"
#import "Constants.h"
#include <time.h>
#import "IntegerSortBenchmarkTest.h"
#import "FibonacciBenchmarkTest.h"
//#import "LagrangeBenchmarkTest.h"
#import "MatrixInversionBenchmarkTest.h"
#import "MatrixMultiplicationBenchmarkTest.h"

@interface BenchmarkTest(Private)

@end

@implementation BenchmarkTest


static int overallCounter = 0;

+(double)fineTime{
    double d = (double)clock();
    return d/CLOCKS_PER_SEC;
}

+(NSArray *)availableTests{
    static NSArray *tests;
    
    if(!tests){
        tests = @[[[IntegerSortBenchmarkTest alloc] initWithSize:BENCHMARK_INTEGER_SORT_ARRAY_SIZE],
                  [[FibonacciBenchmarkTest alloc] initWithSize:
                   BENCHMARK_FIBONACCI_EXTENT],
                  //[[LagrangeBenchmarkTest alloc] init]];
                  [[MatrixInversionBenchmarkTest alloc] initWithSize:BENCHMARK_MATRIX_SIZE],
                  [[MatrixMultiplicationBenchmarkTest alloc] initWithSize:BENCHMARK_MM_SIZE]];
    }
    
    return tests;
}

-(NSString *)testName{
    return nil;
}

-(void)beginTest{
    [self reset];
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
        //Background Thread
        double testStarted = [BenchmarkTest fineTime];
        double testFinished;
        do{
            [self singleTest];
            _counter++;
            testFinished = [BenchmarkTest fineTime];
            
        }while(testFinished - testStarted <  BENCHMARK_TEST_RUNTIME);
        
        dispatch_async(dispatch_get_main_queue(), ^(void){
            completed = YES;
            overallCounter++;
            [_delegate benchmarkTestComplete:self];
        });
    });
}

-(void)reset{
    completed = NO;
    _counter = 0;
}

-(NSString *)testReport{
    if(completed){
        return [NSString stringWithFormat:@"%d %f %@ %d", overallCounter,
                BENCHMARK_TEST_RUNTIME,
                [self testName],
                _counter];
    }
    
    return nil;
}

-(id)initWithSize:(int)s{
    if(self = [super init]){
        completed = NO;
        _size = s;
    }
    
    return self;
}

-(void)singleTest{}


@end
