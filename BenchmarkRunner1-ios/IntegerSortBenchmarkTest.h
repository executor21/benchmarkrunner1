//
//  IntegerSortBenchmarkTest.h
//  BenchmarkRunner1
//
//  Created by Lev Trubov on 4/10/16.
//  Copyright © 2016 Lev Trubov. All rights reserved.
//

#import "BenchmarkTest.h"
#import "Constants.h"

@interface IntegerSortBenchmarkTest : BenchmarkTest{
    int arrayLength;
    int arrayToSort[BENCHMARK_INTEGER_SORT_ARRAY_SIZE];
}

@end
