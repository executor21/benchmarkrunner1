//
//  LagrangeBenchmarkTest.m
//  BenchmarkRunner1
//
//  Created by Lev Trubov on 4/10/16.
//  Copyright © 2016 Lev Trubov. All rights reserved.
//

#import "LagrangeBenchmarkTest.h"

@interface LagrangeBenchmarkTest(Private)

-(double)yLagrange:(double)x;

@end

@implementation LagrangeBenchmarkTest

-(NSString *)testName{
    return @"Lagrange Interpolation";
}

-(id)init{
    if(self = [super init]){
        pt1.x = BENCHMARK_LAGRANGE_PT_1_X;
        pt1.y = BENCHMARK_LAGRANGE_PT_1_Y;
        pt2.x = BENCHMARK_LAGRANGE_PT_2_X;
        pt2.y = BENCHMARK_LAGRANGE_PT_2_Y;
        pt3.x = BENCHMARK_LAGRANGE_PT_3_X;
        pt3.y = BENCHMARK_LAGRANGE_PT_3_Y;
        length = BENCHMARK_LAGRANGE_GRANULARITY;
    }
    
    return self;
}

-(void)singleTest{
    int i = 0;
    int gap = (pt3.x - pt1.x)/length;
    while(i < length){
        double x = pt1.x + i*gap;
        double y = [self yLagrange:x];
        struct BMPoint pi;
        pi.x = x;
        pi.y = y;
        interpolation[i] = pi;
        i++;
    }
}

-(double)yLagrange:(double)x{
    return pt1.y*(x-pt2.x)/(pt1.x-pt2.x)*(x-pt3.x)/(pt1.x-pt3.x)
    + pt2.y*(x-pt1.x)/(pt2.x-pt1.x)*(x-pt3.x)/(pt2.x-pt3.x)
    + pt3.y*(x-pt1.x)/(pt3.x-pt1.x)*(x-pt2.x)/(pt3.x-pt2.x);
}

@end
