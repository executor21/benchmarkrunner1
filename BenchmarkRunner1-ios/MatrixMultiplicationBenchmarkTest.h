//
//  MatrixMultiplicationBenchmarkTest.h
//  BenchmarkRunner1
//
//  Created by Lev Trubov on 6/19/16.
//  Copyright © 2016 Lev Trubov. All rights reserved.
//

#import "BenchmarkTest.h"

@interface MatrixMultiplicationBenchmarkTest : BenchmarkTest{
    int N;
    int **matA, **matB, **matC;
}

@end
