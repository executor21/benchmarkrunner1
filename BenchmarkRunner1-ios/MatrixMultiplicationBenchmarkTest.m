//
//  MatrixMultiplicationBenchmarkTest.m
//  BenchmarkRunner1
//
//  Created by Lev Trubov on 6/19/16.
//  Copyright © 2016 Lev Trubov. All rights reserved.
//

#import "MatrixMultiplicationBenchmarkTest.h"
#import "Constants.h"

@interface MatrixMultiplicationBenchmarkTest(Private)

-(void)fillInitialMatrices;
-(void)multiplyMatrices;

@end

@implementation MatrixMultiplicationBenchmarkTest

-(NSString *)testName{
    return @"Matrix multiplication";
}

-(id)initWithSize:(int)s{
    if(self = [super initWithSize:s]){
        N = s;
        matA = malloc(N*sizeof(int*));
        matB = malloc(N*sizeof(int*));
        matC = malloc(N*sizeof(int*));

        for(int i = 0; i < N; i++){
            matA[i] = malloc(N*sizeof(int));
            matB[i] = malloc(N*sizeof(int));
            matC[i] = malloc(N*sizeof(int));
        }
        
    }
    return self;
}

-(void)dealloc{
    for(int i = 0; i < N; i++){
        free(matA[i]);
        free(matB[i]);
        free(matC[i]);
    }
    
    free(matA);
    free(matB);
    free(matC);
}

-(void)singleTest{
    [self fillInitialMatrices];
    [self multiplyMatrices];
}

-(void)fillInitialMatrices{
    int valA, valB;
    int diff = BENCHMARK_MM_UL - BENCHMARK_MM_LL;
    for(int i = 0; i < N; i++){
        
        valA = BENCHMARK_MM_LL + i%diff;
        valB = BENCHMARK_MM_UL - i%diff;
        
        for(int j = 0; j < N; j++){
            matA[i][j] = valA;
            matB[i][j] = valB;
            
            if(++valA > BENCHMARK_MM_UL){
                valA = BENCHMARK_MM_LL;
            }
            
            if(--valB < BENCHMARK_MM_LL){
                valB = BENCHMARK_MM_UL;
            }
            
            
            matC[i][j] = 0; //just clearing
        }
    }
}

-(void)multiplyMatrices{
    for(int i = 0; i < N; i++){
        for(int j = 0; j < N; j++){
            for(int k = 0; k < N; k++){
                matC[i][j] += matA[i][k]*matB[k][j];
            }
        }
    }
}

@end
