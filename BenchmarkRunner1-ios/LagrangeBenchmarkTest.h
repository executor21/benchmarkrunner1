//
//  LagrangeBenchmarkTest.h
//  BenchmarkRunner1
//
//  Created by Lev Trubov on 4/10/16.
//  Copyright © 2016 Lev Trubov. All rights reserved.
//

#import "BenchmarkTest.h"
#import "Constants.h"

struct BMPoint{
    double x;
    double y;
};

@interface LagrangeBenchmarkTest : BenchmarkTest{
    struct BMPoint pt1, pt2, pt3;
    int length;
    struct BMPoint interpolation[BENCHMARK_LAGRANGE_GRANULARITY];
    
}

@end
