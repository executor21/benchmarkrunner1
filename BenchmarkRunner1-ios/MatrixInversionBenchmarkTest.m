//
//  MatrixInversionBenchmarkTest.m
//  BenchmarkRunner1
//
//  Created by Lev Trubov on 5/19/16.
//  Copyright © 2016 Lev Trubov. All rights reserved.
//

#import "MatrixInversionBenchmarkTest.h"

@interface MatrixInversionBenchmarkTest(Private)

-(void)fillMatrix;
-(void)invertMatrix;

-(double)luDecomp:(int[])index;
-(void)luBackSub:(int[])index with:(double[])b;

@end

@implementation MatrixInversionBenchmarkTest

-(NSString *)testName{
    return @"Matrix inversion";
}

-(id)initWithSize:(int)s{
    if(self = [super initWithSize:s]){
        N = s;
        matrix = malloc(N*sizeof(double*));
        y = malloc(N*sizeof(double*));
        for(int i = 0; i < N; i++){
            matrix[i] = malloc(N*sizeof(double));
            y[i] = malloc(N*sizeof(double));
        }
    }
    
    return self;
}

-(void)dealloc{
    for(int i = 0; i < N; i++){
        free(matrix[i]);
        free(y[i]);
    }
    
    free(matrix);
    free(y);
}

-(void)singleTest{
    [self fillMatrix];
    [self invertMatrix];
    [self invertMatrix];
}

-(void)fillMatrix{
    for(int i = 0; i < N; i++){
        for(int j = 0; j < N; j++){
            if(i == j){
                matrix[i][j] = BENCHMARK_MATRIX_DIAG_VALUE;
            }else{
                matrix[i][j] = BENCHMARK_MATRIX_NON_DIAG_VALUE;
            }
            y[i][j] = 0; //just clearing
        }
    }
}

-(void)invertMatrix{
    int *index = malloc(N*sizeof(int));
    //double y[N][N];
    double *col = malloc(N*sizeof(double));
    
    [self luDecomp:index];
    for(int j = 0; j < N; j++){
        for(int i = 0; i < N; i++) {
            col[i] = 0.0;
        }
        col[j] = 1.0;
        [self luBackSub:index with:col];
        for(int i = 0; i < N; i++){
            y[i][j] = col[i];
        }
    }
    
    //copy y to matrix
    for(int i = 0; i < N; i++){
        for(int j = 0; j < N; j++){
            matrix[i][j] = y[i][j];
        }
    }
    free(index);
    free(col);
    
}

-(double)luDecomp:(int[])index{
    int imax = 0; //initialize to avoid error
    double big,dum,sum,temp;
    double vv[N];
    double d = 1.0;
    
    for(int i = 0; i < N; i++){
        big = 0.0;
        for(int j = 0; j < N; j++){
            if((temp = fabs(matrix[i][j])) > big){
                big = temp;
            }
        }
        if(big == 0){
            NSLog(@"Singular matrix");
            return 0.0;
        }
        vv[i] = 1.0/big;
    }
    
    //loop over columns
    for(int j = 0; j < N; j++){
        for(int i = 0; i < N; i++){
            sum = matrix[i][j];
            for(int k = 0; k < N; k++){
                sum -= matrix[i][k]*matrix[k][j];
            }
            matrix[i][j] = sum;
        }
        big = 0.0;
        for(int i = j; i < N; i++){
            sum = matrix[i][j];
            for(int k = 0; k < j-1; k++){
                sum -= matrix[i][k]*matrix[k][j];
            }
            matrix[i][j] = sum;
            if((dum = vv[i]*fabs(sum)) >= big) {
                big = dum;
                imax = i;
            }
        }
        
        if(j != imax){
            for(int k = 0; k < N; k++){
                dum = matrix[imax][k];
                matrix[imax][k] = matrix[j][k];
                matrix[j][k] = dum;
            }
            d *= -1;
            vv[imax] = vv[j];
        }
        index[j] = imax;
        if(matrix[j][j] == 0.0){
            matrix[j][j] = BENCHMARK_MATRIX_EPS;
        }
        
        if(j != N-1){
            dum = 1.0/matrix[j][j];
            for(int i = j+1; i < N; i++){
                matrix[i][j] *= dum;
            }
        }
    }
    
    return d;
}

-(void)luBackSub:(int[])index with:(double[])b{
    int ii = 0;
    int ip;
    double sum;
    
    for(int i = 0; i < N; i++){
        ip = index[i];
        sum = b[ip];
        b[ip] = b[i];
        if(ii != 0){
            for(int j = ii; j <= i-1; j++){
                sum -= matrix[i][j]*b[j];
            }
        }else if(sum != 0.0){
            ii = i;
        }
        b[i] = sum;
    }
    
    for(int i = N-1; i >= 0; i--){
        sum = b[i];
        for(int j = i+1; j < N; j++){
            sum -= matrix[i][j]*b[j];
        }
        b[i] = sum/matrix[i][i];
    }
}


@end
