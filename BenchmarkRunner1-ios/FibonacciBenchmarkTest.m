//
//  FibonacciBenchmarkTest.m
//  BenchmarkRunner1
//
//  Created by Lev Trubov on 4/10/16.
//  Copyright © 2016 Lev Trubov. All rights reserved.
//

#import "FibonacciBenchmarkTest.h"
#import "Constants.h"

@interface FibonacciBenchmarkTest(Private)

-(int)getFibonacciNumber:(int)index;

@end

@implementation FibonacciBenchmarkTest

-(NSString *)testName{
    return @"Fibonacci Calculator";
}

-(id)initWithSize:(int)s{
    if(self = [super initWithSize:s]){
        extent = s;
    }
    
    return self;
}

-(void)singleTest{
    [self getFibonacciNumber:extent];
}

-(int)getFibonacciNumber:(int)index{
    if(index == 0 || index == 1){
        return index;
    }
    
    return [self getFibonacciNumber:index-1] +
    [self getFibonacciNumber:index-2];
    
}


@end
