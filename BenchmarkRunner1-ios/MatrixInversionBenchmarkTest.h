//
//  MatrixInversionBenchmarkTest.h
//  BenchmarkRunner1
//
//  Created by Lev Trubov on 5/19/16.
//  Copyright © 2016 Lev Trubov. All rights reserved.
//

#import "BenchmarkTest.h"
#import "Constants.h"

@interface MatrixInversionBenchmarkTest : BenchmarkTest {
    int N;
    //double matrix[BENCHMARK_MATRIX_SIZE][BENCHMARK_MATRIX_SIZE];
    double **matrix;
    double **y;
    
}

@end
