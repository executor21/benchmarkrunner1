//
//  IntegerSortBenchmarkTest.m
//  BenchmarkRunner1
//
//  Created by Lev Trubov on 4/10/16.
//  Copyright © 2016 Lev Trubov. All rights reserved.
//

#import "IntegerSortBenchmarkTest.h"

@interface IntegerSortBenchmarkTest(Private)

-(void)fillArrayToSort;

-(void)swapArrayValues:(int[])a at:(int)i and:(int)j;
-(void)quicksortArray:(int[])a from:(int)lo to:(int)hi;
-(int)partitionArray:(int[])a from:(int)lo to:(int)hi;


@end

@implementation IntegerSortBenchmarkTest

-(NSString *)testName{
    return @"Integer Array Sort";
}

-(id)initWithSize:(int)s{
    if(self = [super initWithSize:s]){
        arrayLength = s;
    }
    
    return self;
}

-(void)singleTest{
    [self fillArrayToSort];
    [self quicksortArray:arrayToSort from:0 to:arrayLength-1];
}

-(void)fillArrayToSort{
    int value = BENCHMARK_INTEGER_SORT_LOWER_LIMIT;
    for(int i = 0; i < arrayLength; i++){
        arrayToSort[i] = value;
        if(value < BENCHMARK_INTEGER_SORT_UPPER_LIMIT){
            value++;
        }else{
            value = BENCHMARK_INTEGER_SORT_LOWER_LIMIT;
        }
    }
}

//-----------sorting----------------

-(void)swapArrayValues:(int[])a at:(int)i and:(int)j{
    if(i != j){
        int temp = a[i];
        a[i] = a[j];
        a[j] = temp;
    }
}

-(void)quicksortArray:(int[])a from:(int)lo to:(int)hi{
    if(lo < hi){
        int p = [self partitionArray:a from:lo to:hi];
        [self quicksortArray:a from:lo to:p-1];
        [self quicksortArray:a from:p+1 to:hi];
    }
}

-(int)partitionArray:(int[])a from:(int)lo to:(int)hi{
    int pivot = a[hi];
    int i = lo;
    
    for(int j = lo; j < hi; j++){
        if(a[j] <= pivot){
            [self swapArrayValues:a at:i and:j];
            i++;
        }
    }
    
    [self swapArrayValues:a at:i and:hi];
    //NSLog(@"%d (%d,%d) %d", lo, i, pivot, hi);
    return i;
}



@end
