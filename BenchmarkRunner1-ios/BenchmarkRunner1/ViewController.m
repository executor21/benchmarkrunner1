//
//  ViewController.m
//  BenchmarkRunner1
//
//  Created by Lev Trubov on 4/10/16.
//  Copyright © 2016 Lev Trubov. All rights reserved.
//

#import "ViewController.h"
#import <sys/utsname.h>

//@import Firebase;

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    NSFileManager *fm = [NSFileManager defaultManager];
    
    recordPath = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
    recordPath = [recordPath stringByAppendingPathComponent:@"record.txt"];
    if(![fm fileExistsAtPath:recordPath]){
        [fm createFileAtPath:recordPath contents:[[NSData alloc] init] attributes:nil];
    }
    
    [self resetFileWriter];
    
    [recordUpdate writeData:[[self initialData] dataUsingEncoding:NSUTF8StringEncoding]];
    [recordUpdate writeData:[@"\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSLog(@"%@", recordPath);
}

-(void)resetFileWriter{
    if(recordUpdate){
        [recordUpdate closeFile];
    }
    
    recordUpdate = [NSFileHandle fileHandleForUpdatingAtPath:recordPath ];
    [recordUpdate seekToEndOfFile];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSString *)initialData{
    return [NSString stringWithFormat:@"%@ %@",
            [[NSDate alloc] init],
            [self deviceName]
            ];
}

-(NSString*) deviceName{
    struct utsname systemInfo;
    uname(&systemInfo);
    
    return [NSString stringWithCString:systemInfo.machine
                              encoding:NSUTF8StringEncoding];
}

//--------------actions-------------

-(IBAction)runSelectedTest:(id)sender{
    NSUInteger testIndex = [_testPicker selectedRowInComponent:0];
    BenchmarkTest *test = [BenchmarkTest availableTests][testIndex];
    test.delegate = self;
    
    alert = [UIAlertController alertControllerWithTitle:nil
                                                                   message:@"Please wait\n\n\n"
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIActivityIndicatorView *spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    spinner.center = CGPointMake(130.5, 65.5);
    spinner.color = [UIColor blackColor];
    [spinner startAnimating];
    [alert.view addSubview:spinner];
    [self presentViewController:alert animated:NO completion:nil];
    
    counter = -1;
    [test beginTest];
    
}

-(IBAction)runSelectedTest10:(id)sender{
    NSUInteger testIndex = [_testPicker selectedRowInComponent:0];
    BenchmarkTest *test = [BenchmarkTest availableTests][testIndex];
    test.delegate = self;
    
    alert = [UIAlertController alertControllerWithTitle:nil
                                                message:@"Please wait\n\n\n"
                                         preferredStyle:UIAlertControllerStyleAlert];
    
    UIActivityIndicatorView *spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    spinner.center = CGPointMake(130.5, 65.5);
    spinner.color = [UIColor blackColor];
    [spinner startAnimating];
    [alert.view addSubview:spinner];
    [self presentViewController:alert animated:NO completion:nil];
    
    counter = 10;
    [test beginTest];
    
}

-(IBAction)emailTestResults:(id)sender{
    MFMailComposeViewController *mcc = [[MFMailComposeViewController alloc] init];
    mcc.mailComposeDelegate = self;
    [mcc setSubject:@"Benchmarking results"];
    NSData *data = [[NSFileManager defaultManager] contentsAtPath:recordPath];
    [mcc addAttachmentData:data mimeType:@"text/plain" fileName:@"bm_record.txt"];

    
    [self presentViewController:mcc animated:YES  completion:nil];
}

//--------------interface---------------

-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    return [[BenchmarkTest availableTests] count];
}

- (NSString *)pickerView:(UIPickerView *)pickerView
             titleForRow:(NSInteger)row
            forComponent:(NSInteger)component{
    BenchmarkTest *test = [BenchmarkTest availableTests][row];
    return [test testName];
}

-(void)benchmarkTestComplete:(BenchmarkTest *)test{
    //[self uploadTestResults:test];
    [recordUpdate writeData:[[test testReport] dataUsingEncoding:NSUTF8StringEncoding]];
    [recordUpdate writeData:[@"\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    if(counter <= 0){
        if(counter < 0){
            _resultLabel.text = [NSString stringWithFormat:@"%d", test.counter];
        }else{
            int rMin = results[0];
            int rMax = results[0];
            int sum = results[0];
            for(int i = 0; i < 10; i++){
                int curr = results[i];
                if(curr > rMax){
                    rMax = curr;
                }
                
                if(curr < rMin){
                    rMin = curr;
                }
                
                sum += curr;
            }
            
            sum -= rMin;
            sum -= rMax;
            
            double avg = ((double)sum)/8.0;
            _resultLabel.text = [NSString stringWithFormat:@"%f", avg];
        }
        [alert dismissViewControllerAnimated:YES completion:nil];
        [self resetFileWriter];
    }else{
        results[10-counter] = test.counter;
        counter--;
        [test reset];
        [test beginTest];
    }
}

/*
-(void)uploadTestResults:(BenchmarkTest *)test{
    NSString *devID = [[UIDevice currentDevice] identifierForVendor].UUIDString;
#if TARGET_OS_SIMULATOR
    devID = @"E621E1F8-C36C-495A-74AB-0C247A3E6E5F";
#endif
    
    NSDictionary *testInfo = @{@"system" : devID,
                               @"test" : [test testName],
                               @"size" : [NSNumber numberWithInt:test.size],
                               @"result" : [NSNumber numberWithInt:test.counter]};
    FIRDatabaseReference *ref = [[FIRDatabase database] reference];
    [[[ref child:@"tests"] childByAutoId] setValue:testInfo];
}//*/

-(void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error{
    [controller dismissViewControllerAnimated:YES completion:nil];
}

@end
