//
//  ViewController.h
//  BenchmarkRunner1
//
//  Created by Lev Trubov on 4/10/16.
//  Copyright © 2016 Lev Trubov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BenchmarkTest.h"
#import <MessageUI/MessageUI.h>

@interface ViewController : UIViewController <UIPickerViewDataSource,UIPickerViewDelegate, BenchmarkTestDelegate,MFMailComposeViewControllerDelegate>{
    NSString *recordPath;
    NSFileHandle *recordUpdate;
    UIAlertController *alert;
    int counter;
    int results[10];
}

@property(weak) IBOutlet UILabel *resultLabel;
@property(weak) IBOutlet UIPickerView *testPicker;

-(IBAction)runSelectedTest:(id)sender;
-(IBAction)runSelectedTest10:(id)sender;
-(IBAction)emailTestResults:(id)sender;

@end

