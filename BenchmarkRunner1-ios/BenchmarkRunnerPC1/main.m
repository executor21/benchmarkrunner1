//
//  main.m
//  BenchmarkRunnerPC1
//
//  Created by Lev Trubov on 4/10/16.
//  Copyright © 2016 Lev Trubov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BenchmarkExecutable.h"

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        // insert code here...
        BenchmarkExecutable *be = [[BenchmarkExecutable alloc] init];
        [be runTests];
    }
    return 0;
}
