//
//  BenchmarkExecutable.m
//  BenchmarkRunner1
//
//  Created by Lev Trubov on 4/10/16.
//  Copyright © 2016 Lev Trubov. All rights reserved.
//

#import "BenchmarkExecutable.h"
#import <sys/utsname.h>

@implementation BenchmarkExecutable

-(id)init{
    if(self = [super init]){
        record = fopen("bm_record.txt", "a");
        fprintf(record, [self initialData]);
        fprintf(record, "\n");
    }
    
    return self;
}

-(void)runTests{
    NSArray *tests = [BenchmarkTest availableTests];
    for(BenchmarkTest *t in tests){
        t.delegate = self;
        int results[10];
        for(int i = 0; i < 10; i++){
            [t beginTest];
            CFRunLoopRun();
            results[i] = t.counter;
            [t reset];
        }
        
        int rMin = results[0];
        int rMax = results[0];
        int sum = results[0];
        for(int i = 0; i < 10; i++){
            int curr = results[i];
            if(curr > rMax){
                rMax = curr;
            }
            
            if(curr < rMin){
                rMin = curr;
            }
            
            sum += curr;
        }
        
        sum -= rMin;
        sum -= rMax;
        
        double avg = ((double)sum)/8.0;
        
        NSLog(@"%@ %f", t.testName, avg);
    }
    
    fclose(record);
}

-(void)benchmarkTestComplete:(BenchmarkTest *)test{
    
    NSString *testResult = [test testReport];
    //NSLog(@"%@", testResult);
    
    fprintf(record, [testResult cStringUsingEncoding:NSUTF8StringEncoding]);
    fprintf(record, "\n");
    //testComplete = YES;
    CFRunLoopStop(CFRunLoopGetMain());
}

-(const char *)initialData{
    return [[NSString stringWithFormat:@"%@ %@",
                   [[NSDate alloc] init],
                   [self deviceName]
                   ] cStringUsingEncoding:NSUTF8StringEncoding];
}

-(NSString*) deviceName{
    struct utsname systemInfo;
    uname(&systemInfo);
    
    return [NSString stringWithCString:systemInfo.machine
                              encoding:NSUTF8StringEncoding];
}

@end
