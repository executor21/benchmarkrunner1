//
//  BenchmarkExecutable.h
//  BenchmarkRunner1
//
//  Created by Lev Trubov on 4/10/16.
//  Copyright © 2016 Lev Trubov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BenchmarkTest.h"

@interface BenchmarkExecutable : NSObject <BenchmarkTestDelegate>{
    FILE *record;
}

-(void)runTests;
-(const char *)initialData;
-(NSString *)deviceName;

@end
