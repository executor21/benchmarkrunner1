//
//  BenchmarkTest.h
//  BenchmarkRunner1
//
//  Created by Lev Trubov on 4/10/16.
//  Copyright © 2016 Lev Trubov. All rights reserved.
//

#import <Foundation/Foundation.h>

@class BenchmarkTest;

@protocol BenchmarkTestDelegate <NSObject>

-(void)benchmarkTestComplete:(BenchmarkTest *)test;

@end

@interface BenchmarkTest : NSObject{
    BOOL completed;
}

@property(weak) id<BenchmarkTestDelegate> delegate;
@property(readonly) int counter;
@property(readonly) int size;

-(id)initWithSize:(int)s;

+(NSArray *)availableTests;
-(NSString *)testName;

-(void)beginTest;
-(void)reset;
-(NSString *)testReport;

-(void)singleTest;

@end
