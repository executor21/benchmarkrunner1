package masters.lev.sfsu.edu.benchmarkrunner1;
import java.util.List;

import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.UUID;

import android.util.Log;

public class MainActivity extends AppCompatActivity implements BenchmarkTestCallback {

    private ProgressDialog waitAlert;
    private int counter;
    private int [] results = new int[10];

    /*
    private FirebaseAuth mAuth;
    private DatabaseReference mDatabase;
    private String deviceID;*/

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /*
        mAuth = FirebaseAuth.getInstance();
        //*
        mAuth.signInWithEmailAndPassword("ltrubov+fb@gmail.com", "Ttsn2G2f2WlT").addOnCompleteListener(
                this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if(task.isSuccessful()){
                            SharedPreferences pref = getPreferences(MODE_PRIVATE);
                            String key = "unique_id";
                            String empty = "EMPTY";
                            deviceID = pref.getString(key, empty);
                            if(deviceID.equals(empty)) {
                                SharedPreferences.Editor edit = pref.edit();
                                UUID id = UUID.randomUUID();
                                deviceID = id.toString();
                                edit.putString(key, deviceID);
                                edit.apply();
                            }

                            mDatabase = FirebaseDatabase.getInstance().getReference();
                            mDatabase.child("systems").child(deviceID).addListenerForSingleValueEvent(
                                    new ValueEventListener() {
                                        @Override
                                        public void onDataChange(DataSnapshot dataSnapshot) {
                                            if(dataSnapshot == null){
                                                FBDBSystem sys = new FBDBSystem("android", Build.MODEL);
                                                mDatabase.child("systems").child(deviceID).setValue(sys);
                                            }
                                        }

                                        @Override
                                        public void onCancelled(DatabaseError databaseError) {
                                            Log.d("FB", "DB op cancelled");
                                        }
                                    }
                            );


                        }else{
                            System.out.println("xxx");
                            Log.d("FB", "Authorization failed");
                        }
                    }
                });

        //*/

        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Spinner spinner = (Spinner)findViewById(R.id.spinner);
                int pos = spinner.getSelectedItemPosition();
                if(pos != AdapterView.INVALID_POSITION){
                    waitAlert = ProgressDialog.show(MainActivity.this, "Please Wait", null);

                    BenchmarkTest bt = BenchmarkTest.availableTests()[pos];
                    counter = 10;
                    bt.beginTest(MainActivity.this);
                }
            }
        });

        FloatingActionButton fab2 = (FloatingActionButton) findViewById(R.id.fab2);
        fab2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Spinner spinner = (Spinner)findViewById(R.id.spinner);
                int pos = spinner.getSelectedItemPosition();
                if(pos != AdapterView.INVALID_POSITION){
                    waitAlert = ProgressDialog.show(MainActivity.this, "Please Wait", null);

                    BenchmarkTest bt = BenchmarkTest.availableTests()[pos];
                    counter = -1;
                    bt.beginTest(MainActivity.this);
                }
            }
        });

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, availableTestNames());
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        Spinner spinner = (Spinner)findViewById(R.id.spinner);
        spinner.setAdapter(dataAdapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public List<String> availableTestNames(){
        ArrayList<String> res = new ArrayList<>();
        for(BenchmarkTest bt : BenchmarkTest.availableTests()){
            res.add(bt.getName());
        }

        return res;
    }

    public void benchmarkTestCompleted(BenchmarkTest test){
        //uploadTestResults(test);
        if(counter <= 0){
            TextView tv = (TextView) findViewById(R.id.textView);
            if(counter < 0){
                tv.setText("" + test.getCounter());
            }else{
                int rMin = results[0];
                int rMax = results[0];
                int sum = results[0];
                for(int i = 0; i < 10; i++){
                    int curr = results[i];
                    if(curr > rMax){
                        rMax = curr;
                    }

                    if(curr < rMin){
                        rMin = curr;
                    }

                    sum += curr;
                }

                sum -= rMin;
                sum -= rMax;

                double avg = ((double)sum)/8.0;

                tv.setText("" + avg);
            }

            waitAlert.dismiss();


        }else{
            results[10-counter] = test.getCounter();
            counter--;
            test.reset();
            test.beginTest(this);
        }


    }

    /*
    public void uploadTestResults(BenchmarkTest test){
        FBDBTest submission = new FBDBTest(deviceID, test.testName, test.getSize(), test.getCounter());
        String subKey = mDatabase.child("tests").push().getKey();
        mDatabase.child("tests").child(subKey).setValue(submission);
    }//*/
}
