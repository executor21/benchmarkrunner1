package masters.lev.sfsu.edu.benchmarkrunner1;

/**
 * Created by lev on 6/19/16.
 */
public class MatrixMultiplicationTest extends BenchmarkTest {

    private int[][] matA, matB;
    private int N;

    public MatrixMultiplicationTest(int s){
        super(s);
        N = s;
        testName = "Matrix multiplication";
        matA = new int[N][N];
        matB = new int[N][N];
    }

    @Override
    public void singleTest() {
        fillInitialMatrices();
        multiplyMatrices();
    }

    private void fillInitialMatrices(){
        int valA, valB;
        int diff = BenchmarkConstants.BENCHMARK_MM_UL - BenchmarkConstants.BENCHMARK_MM_LL;

        for(int i = 0; i < N; i++){
            valA = BenchmarkConstants.BENCHMARK_MM_LL + i%diff;
            valB = BenchmarkConstants.BENCHMARK_MM_UL - i%diff;
            for(int j = 0; j < N; j++){
                matA[i][j] = valA;
                matB[i][j] = valB;

                if(++valA > BenchmarkConstants.BENCHMARK_MM_UL){
                    valA = BenchmarkConstants.BENCHMARK_MM_LL;
                }

                if(--valB < BenchmarkConstants.BENCHMARK_MM_LL) {
                    valB = BenchmarkConstants.BENCHMARK_MM_UL;
                }
            }
        }
    }

    private void multiplyMatrices(){
        int[][] matC = new int[N][N];
        for(int i = 0; i < N; i++){
            for(int j = 0; j < N; j++){
                for(int k = 0; k < N; k++){
                    matC[i][j] += matA[i][k]*matB[k][j];
                }
            }
        }

    }
}
