package masters.lev.sfsu.edu.benchmarkrunner1;

/**
 * Created by lev on 5/19/16.
 */
public class MatrixInversionTest extends BenchmarkTest {

    private double[][] matrix;
    private int N;

    public MatrixInversionTest(int s){
        super(s);
        N = s;
        testName = "Matrix Inversion";
        matrix = new double[N][N];
    }

    @Override
    public void singleTest() {
        fillMatrix();
        invertMatrix();
        invertMatrix(); //back to original
    }

    private void fillMatrix(){
        for(int i = 0; i < N; i++){
            for(int j = 0; j < N; j++){
                if(i == j){
                    matrix[i][j] = BenchmarkConstants.BENCHMARK_MATRIX_DIAG_VALUE;
                }else{
                    matrix[i][j] = BenchmarkConstants.BENCHMARK_MATRIX_NON_DIAG_VALUE;
                }
            }
        }
    }

    private void invertMatrix(){
        int[] index = new int[N];
        double[][] y = new double[N][N];
        double[] col = new double[N];

        luDecomp(index);
        for(int j = 0; j < N; j++){
            for(int i = 0; i < N; i++) {
                col[i] = 0.0;
            }
            col[j] = 1.0;
            luBackSub(index, col);
            for(int i = 0; i < N; i++){
                y[i][j] = col[i];
            }
        }

        matrix = y;
    }

    private double luDecomp(int[] index){ //changes matrix to lu-decomposed, returns +1.0 or -1.0
        int imax = 0; //initialize to avoid error
        double big,dum,sum,temp;
        double[] vv = new double[N];
        double d = 1.0;

        for(int i = 0; i < N; i++){
            big = 0.0;
            for(int j = 0; j < N; j++){
                if((temp = Math.abs(matrix[i][j])) > big){
                    big = temp;
                }
            }
            if(big == 0){
                System.out.println("Singular matrix");
                return 0.0;
            }
            vv[i] = 1.0/big;
        }

        //loop over columns
        for(int j = 0; j < N; j++){
            for(int i = 0; i < N; i++){
                sum = matrix[i][j];
                for(int k = 0; k < N; k++){
                    sum -= matrix[i][k]*matrix[k][j];
                }
                matrix[i][j] = sum;
            }
            big = 0.0;
            for(int i = j; i < N; i++){
                sum = matrix[i][j];
                for(int k = 0; k < j-1; k++){
                    sum -= matrix[i][k]*matrix[k][j];
                }
                matrix[i][j] = sum;
                if((dum = vv[i]*Math.abs(sum)) >= big) {
                    big = dum;
                    imax = i;
                }
            }

            if(j != imax){
                for(int k = 0; k < N; k++){
                    dum = matrix[imax][k];
                    matrix[imax][k] = matrix[j][k];
                    matrix[j][k] = dum;
                }
                d *= -1;
                vv[imax] = vv[j];
            }
            index[j] = imax;
            if(matrix[j][j] == 0.0){
                matrix[j][j] = BenchmarkConstants.BENCHMARK_MATRIX_EPS;
            }

            if(j != N-1){
                dum = 1.0/matrix[j][j];
                for(int i = j+1; i < N; i++){
                    matrix[i][j] *= dum;
                }
            }
        }

        return d;
    }

    private void luBackSub(int[] index, double[] b){ //b is input as right-hand vector and output
                                                     //as solution
        int ii = 0;
        int ip;
        double sum;

        for(int i = 0; i < N; i++){
            ip = index[i];
            sum = b[ip];
            b[ip] = b[i];
            if(ii != 0){
                for(int j = ii; j <= i-1; j++){
                    sum -= matrix[i][j]*b[j];
                }
            }else if(sum != 0.0){
                ii = i;
            }
            b[i] = sum;
        }

        for(int i = N-1; i >= 0; i--){
            sum = b[i];
            for(int j = i+1; j < N; j++){
                sum -= matrix[i][j]*b[j];
            }
            b[i] = sum/matrix[i][i];
        }


    }


}
