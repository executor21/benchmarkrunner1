package masters.lev.sfsu.edu.benchmarkrunner1;

/**
 * Created by lev on 5/17/16.
 */
public class IntegerSortBenchmarkTest extends BenchmarkTest {
    private int[] arrayToSort;

    public IntegerSortBenchmarkTest(int s){
        super(s);
        testName = "Integer Array Sort";
        arrayToSort = new int[s];

    }

    @Override
    public void singleTest() {
        fillArrayToSort();
        quicksortArray(0,arrayToSort.length-1);
    }

    private void fillArrayToSort(){
        int value = BenchmarkConstants.BENCHMARK_INTEGER_SORT_LOWER_LIMIT;
        for(int i = 0; i < arrayToSort.length; i++){
            arrayToSort[i] = value;
            if(value < BenchmarkConstants.BENCHMARK_INTEGER_SORT_UPPER_LIMIT){
                value++;
            }else{
                value = BenchmarkConstants.BENCHMARK_INTEGER_SORT_LOWER_LIMIT;
            }
        }
    }

    private void swapArrayValues(int i, int j){
        if(i != j){
            int temp = arrayToSort[i];
            arrayToSort[i] = arrayToSort[j];
            arrayToSort[j] = temp;
        }
    }

    private int partitionArray(int lo, int hi){
        int pivot = arrayToSort[hi];
        int i = lo;

        for(int j = lo; j < hi; j++){
            if(arrayToSort[j] <= pivot){
                swapArrayValues(i,j);
                i++;
            }
        }

        swapArrayValues(i,hi);
        //System.out.println("" + lo + " (" + i + "," + pivot + ") " + hi);
        return i;
    }

    private void quicksortArray(int lo, int hi){
        if(lo < hi){
            int p = partitionArray(lo,hi);
            //System.out.println("" + lo + " " + p + " " + hi);
            quicksortArray(lo,p-1);
            quicksortArray(p+1,hi);
        }
    }
}
