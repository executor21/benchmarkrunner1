package masters.lev.sfsu.edu.benchmarkrunner1;

/**
 * Created by lev on 5/17/16.
 */
public class FibonacciBenchmarkTest extends BenchmarkTest {

    private int extent;

    public FibonacciBenchmarkTest(int s){
        super(s);
        extent = s;
        testName = "Fibonacci Calculator";
    }

    @Override
    public void singleTest() {
        getFibonacciNumber(extent);
    }

    private int getFibonacciNumber(int n){
        if(n == 0 || n == 1){
            return n;
        }

        return getFibonacciNumber(n-1) + getFibonacciNumber(n-2);
    }
}
