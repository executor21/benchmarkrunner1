package masters.lev.sfsu.edu.benchmarkrunner1;

/**
 * Created by lev on 5/17/16.
 */
public class BenchmarkConstants {
    public static double BENCHMARK_TEST_RUNTIME = 10.0;
    public static int BENCHMARK_INTEGER_SORT_ARRAY_SIZE = 100000;
    public static int BENCHMARK_INTEGER_SORT_LOWER_LIMIT = -100;
    public static int BENCHMARK_INTEGER_SORT_UPPER_LIMIT = 100;
    public static int BENCHMARK_FIBONACCI_EXTENT = 35;
    public static int BENCHMARK_MATRIX_SIZE = 200;
    public static double BENCHMARK_MATRIX_DIAG_VALUE = 2.001;
    public static double BENCHMARK_MATRIX_NON_DIAG_VALUE = 1.001;
    public static double BENCHMARK_MATRIX_EPS = 0.0000001;
    public static int BENCHMARK_MM_SIZE = 200;
    public static int BENCHMARK_MM_LL = -100;
    public static int BENCHMARK_MM_UL = 100;
}
