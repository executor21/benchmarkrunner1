package masters.lev.sfsu.edu.benchmarkrunner1;
import android.os.Looper;
import android.os.Handler;
/**
 * Created by lev on 4/10/16.
 */
public abstract class BenchmarkTest {
    private boolean completed;
    private int counter;
    protected String testName;
    private int size;

    private static BenchmarkTest[] tests = null;
    private static int overallCounter = 0;

    public int getCounter(){
        return counter;
    }
    public int getSize() {return size;}

    public static BenchmarkTest[] availableTests(){
        if(tests == null){
            tests = new BenchmarkTest[]{new FibonacciBenchmarkTest(BenchmarkConstants.BENCHMARK_FIBONACCI_EXTENT),
                    new IntegerSortBenchmarkTest(BenchmarkConstants.BENCHMARK_INTEGER_SORT_ARRAY_SIZE),
                    new MatrixInversionTest(BenchmarkConstants.BENCHMARK_MATRIX_SIZE),
                    new MatrixMultiplicationTest(BenchmarkConstants.BENCHMARK_MM_SIZE)};
        }

        return tests;
    }

    public BenchmarkTest(int s){
        size = s;
    }

    public String getName(){
        return testName;
    }

    public void beginTest(final BenchmarkTestCallback callback){
        reset();

        new Thread(new Runnable() {
            @Override
            public void run() {
                double wantedRunTime =
                        BenchmarkConstants.BENCHMARK_TEST_RUNTIME*1e9;
                double testStarted = System.nanoTime();
                double testFinished;
                do{
                    singleTest();
                    counter++;
                    testFinished = System.nanoTime();

                }while(testFinished - testStarted < wantedRunTime);

                Handler uiHandler = new Handler(Looper.getMainLooper());
                uiHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        completed = true;
                        overallCounter++;
                        callback.benchmarkTestCompleted(BenchmarkTest.this);

                    }
                });


            }
        }).start();
    }

    public void reset(){
        counter = 0;
        completed = false;
    }

    public String testReport(){
        if(completed) {
            return "" + overallCounter + " " +
                    BenchmarkConstants.BENCHMARK_TEST_RUNTIME + " " +
                    testName + " " +
                    counter;
        }

        return "";
    }

    public abstract void singleTest();
}
