package masters.lev.sfsu.edu.benchmarkrunner1;

/**
 * Created by lev on 6/30/16.
 */
public class FBDBTest {
    public String system;
    public String test;
    public int size;
    public int result;

    public FBDBTest(){}

    public FBDBTest(String system, String test, int size, int result){
        this.system = system;
        this.test = test;
        this.size = size;
        this.result = result;
    }
}
