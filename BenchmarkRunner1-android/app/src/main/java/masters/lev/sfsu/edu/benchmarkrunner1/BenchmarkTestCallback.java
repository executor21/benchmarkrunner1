package masters.lev.sfsu.edu.benchmarkrunner1;

/**
 * Created by lev on 5/17/16.
 */
public interface BenchmarkTestCallback {
    public void benchmarkTestCompleted(BenchmarkTest test);
}
